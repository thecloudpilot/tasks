terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "3.47.0"
    }
  }
}

provider "aws"{
    region = "ap-south-1"
    access_key= "<ACCESS KEY>"
    secret_key= "<SECRET KEY>"
}

resource "aws_iam_user" "<RESOURCENAME>" {
  name = "<USERNAME>"
}

resource "aws_iam_user_policy" "policy" {
  name        = "test_policy"
  user = aws_iam_user.<RESOURCENAME>.name

  #S3 FULL ACCESS
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        "Effect": "Allow",
        "Action": "s3:*",
        "Resource": "*"
      },
    ]
  })
}


resource "aws_s3_bucket" "<RESOURCENAME>" {
  bucket = "<BUCKETNAME>"
  acl    = "public-read"
}

resource "aws_instance" "<RESOURCENAME>" {
  ami = "ami-011c99152163a87ae"
  instance_type = "t2.micro"
  key_name = "<PEM FILE NAME>"

  connection {
    type     = "ssh"
    user     = "ec2-user"
    private_key = file("PEM FILE LOCATION HERE")
    host = self.public_ip
  }

  provisioner "remote-exec" {
    inline = [
      "sudo amazon-linux-extras install -y nginx1.12",
      "sudo systemctl start nginx"
    ]
  }
}

#To view the address of the instance
output "ipaddress" {
  value = aws_instance.myinstance.public_ip
}

